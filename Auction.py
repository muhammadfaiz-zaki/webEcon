#!/usr/bin/python
import Bid, Slot

class Auction:
	'This class represents an auction of multiple ad slots to multiple advertisers'
	query = ""
	bids = []

	def __init__(self, term, bids1=[]):
		self.query = term
		
		for b in bids1:
			j=0
			print len(self.bids)
			while j<len(self.bids) and float(b.value) <float(self.bids[j].value):
				j+=1
			self.bids.insert(j,b)


	'''
	This method accepts a Vector of slots and fills it with the results
	of a VCG auction. The competition for those slots is specified in the bids Vector.
	@param slots a Vector of Slots, which (on entry) specifies only the clickThruRates
	and (on exit) also specifies the name of the bidder who won that slot,
	the price said bidder must pay,
	and the expected profit for the bidder.  
	'''

	def executeVCG(self,slots):
		# TODO: implement this method
		size = len(slots)
		
		if size == len(self.bids):
			for i in range(size-1, 0-1, -1):
				if i == size-1:
					slots[i].price = 0
					slots[i].profit = (self.bids[i].value - slots[i].price) * slots[i].clickThruRate
				else:
					slots[i].price = (slots[i].clickThruRate - slots[i + 1].clickThruRate) * self.bids[i+1].value + slots[i+1].price
					slots[i].profit = (self.bids[i].value - self.bids[i+1].value) * slots[i].clickThruRate
				slots[i].bidder = self.bids[i].name
		
		elif size > len(self.bids):
			#how many empty slots?			
			vacant = size - len(self.bids)
			
			start = size-1-vacant

			for i in range(start, 0-1, -1):
				if i == start:
					slots[i].price = 0
					slots[i].profit = (self.bids[i].value - slots[i].price) * slots[i].clickThruRate
				else:
					slots[i].price = (slots[i].clickThruRate - slots[i + 1].clickThruRate) * self.bids[i+1].value + slots[i+1].price
					slots[i].profit = (self.bids[i].value - self.bids[i+1].value) * slots[i].clickThruRate
				slots[i].bidder = self.bids[i].name

		else:
			for i in range(size-1, 0-1, -1):
				if i == size-1:
					slots[i].price = (slots[i].clickThruRate - 0) * self.bids[i+1].value + 0
					slots[i].profit = (self.bids[i].value - self.bids[i+1].value) * slots[i].clickThruRate
				else:
					slots[i].price = (slots[i].clickThruRate - slots[i + 1].clickThruRate) * self.bids[i+1].value + slots[i+1].price
					slots[i].profit = (self.bids[i].value - self.bids[i+1].value) * slots[i].clickThruRate
				slots[i].bidder = self.bids[i].name

