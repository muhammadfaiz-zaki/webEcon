'''
Created on Feb 28, 2016

@author: mfaizmzaki
'''

import numpy as np
import helper
import pylab as plt


#load dataset into array
dataset = np.genfromtxt("spambase.data.csv", delimiter=",")

#shuffle dataset if using online learner. Uncomment to shuffle.
#np.random.shuffle(dataset)

size = dataset.shape[0]
feature = dataset[:,0:dataset.shape[1]-1]
label = dataset[:,dataset.shape[1]-1].reshape(size,1)

#normalize feature (z-score)
mu = np.mean(feature, 0)
sd = np.std(feature, 0)
feature = (feature - mu)/sd

# #add extra col of 1 
feature = np.hstack((np.ones((feature.shape[0],1)),feature))

folds = 10

for i in range(folds):
    #dataset
    testing_X = feature[i::folds]
    testing_Y = label[i::folds]
    training_X = helper.crossV(feature, i, size,folds)
    training_Y = helper.crossV(label,i, size,folds)
     
    #initialize theta. multiple theta initialization for plotting different learners on same plot
    theta = np.zeros((training_X.shape[1],1))
    #theta2 = np.zeros((training_X.shape[1],1))
    #theta3 = np.zeros((training_X.shape[1],1))
    
    #run gradient descent and build model
    #choose between stocGradientDescent or gradientDescent / S = sigmoid L = linear regression
    choice = "L"
     
    theta, cost, mse = helper.gradientDescent(training_X, training_Y, 0.01, 500, theta, choice)
    #theta2, cost2, mse2 = helper.stocGradientDescent(training_X, training_Y, 0.0001, 1000, theta2, choice)
    #theta3, cost3, mse3 = helper.gradientDescent(training_X, training_Y, 0.0003, 1000, theta3, choice)
    
    #plot MSE against iteration graph. Uncomment to plot
    
#     if i == 0:
#         plt.plot(np.arange(1000), mse,'r', label='0.00001')   
#         plt.plot(np.arange(1000), mse2,'b',label='0.0001')  
#         plt.plot(np.arange(1000), mse3,'g',label='0.0003')        
#         plt.xlabel('Iteration')
#         plt.ylabel('MSE')
#         plt.legend(loc='upper right')
#         plt.title('MSE')
#         plt.show()

    #prediction on test set using the built model on various threshold values
    threshold = np.array([0.8,0.7,0.6,0.5,0.4,0.3,0.2])
    tpRate = [0.0]
    fpRate = [0.0]
    temp = []
         
    for k in threshold:
        
        #choose between logistic or linear by uncommenting accordingly        
        testPredict = np.dot(testing_X,theta)
        #testPredict = helper.sigmoid(np.dot(testing_X,theta3))
        testPredict2 = helper.thresholdClass(testPredict, k) 
            
        countPos = 0
        countNeg = 0
            
        pos = sum(testing_Y == 1)
        neg = sum(testing_Y == 0)
            
        #calculation of TPR and FPR
        for j in range(testing_Y.size):
            if testPredict2[j] == 1 and testPredict2[j] == testing_Y[j]:
                countPos += 1.0
            if testPredict2[j] == 1 and testPredict2[j] != testing_Y[j]:
                countNeg += 1.0
                
        tpRate.append(countPos/pos)
        fpRate.append(countNeg/neg)       
    
    #append TPR and FPR of 1.0 at the end for ROC plotting    
    tpRate.append(1.0)
    fpRate.append(1.0)
    
    #calculate AUC
    for l in range(1, len(tpRate)):
        tempAuc = (fpRate[l] - fpRate[l-1]) * (tpRate[l] + tpRate[l-1])
        temp.append(tempAuc)
   
    auc = sum(temp) / 2
    
    #plot ROC curve         
    if i == 0:
        print fpRate
        print tpRate
        print "AUC:"
        print auc
        plt.plot(fpRate, tpRate)
        plt.xlabel('FPR')
        plt.ylabel('TPR')
        plt.title('ROC Curve')
        plt.show() 
             


