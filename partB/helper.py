'''
Created on Feb 28, 2016

@author: mfaizmzaki
'''

import numpy as np

'''
 arrayName is the parent array
 x is the index of iteration
 y is the size of dataset
 10 is step size
'''
def crossV(arrayName,x,y,folds):
    delElement = np.arange(x,y,folds)
    newArray = np.delete(arrayName, delElement, 0)
    return newArray

#convert prediction by threshold value
def thresholdClass(prediction, threshold):
    
    for i in range(prediction.size):
        if prediction[i] > threshold:
            prediction[i] = 1
        else:
            prediction[i] = 0
    
    return prediction        
            
def costFunction(features, trueLabel, theta):
    
    m = trueLabel.size
    prediction = np.dot(features,theta)
            
    #Calculate square errors
    sqErr = (prediction-trueLabel)**2
    
    #Cost function, return scalar
    cost = (1.0/2*m) * sqErr.sum()
    
    return cost

def gradientDescent(X, trueLabel, learningRate, epoch, theta, choice):
    
    cost = np.zeros((epoch,1))
    mse = np.zeros((epoch,1))
    m = trueLabel.size
    
    for i in range(epoch):
        #choose between logistic or linear by uncommenting accordingly 
        prediction = np.dot(X,theta)
        #prediction = sigmoid(np.dot(X,theta))
       
        theta_size = theta.size

        err = (prediction - trueLabel) * X
        errSum = np.sum(err, 0)
        theta = theta - learningRate * (1.0 / m) * errSum.reshape(theta_size,1)
        
        if choice == "L":
            cost[i] = costFunction(X, trueLabel, theta)
        if choice == "S":
            cost[i] = costSigmoid(X, trueLabel, theta)
            mse[i] = costFunction(X, trueLabel, theta)   
               
    return theta, cost, mse

    
def stocGradientDescent(X, trueLabel, learningRate, epoch, theta, choice):    
      
    cost = np.zeros((epoch,1))
    mse = np.zeros((epoch,1))
    m = trueLabel.size
    theta_size = theta.size
    for i in range(epoch):
        
        for j in range(m):
            #choose between logistic or linear by uncommenting accordingly 
            prediction = np.dot(X[j,:],theta)
            #prediction = sigmoid(np.dot(X[j,:],theta))
            err = (prediction - trueLabel[j]) * X[j,:]
            theta = theta - learningRate * (1.0 / m) * err.reshape(theta_size,1)
            
            if choice == "L":
                cost[i] = costFunction(X, trueLabel, theta)
            if choice == "S":
                cost[i] = costSigmoid(X, trueLabel, theta)
                mse[i] = costFunction(X, trueLabel, theta)
            
               
    return theta, cost, mse

#sigmoid function for logistic regression
def sigmoid(prediction):
    
    return 1 / (1 + np.exp(-prediction))

#Cost function for logistic regression
def costSigmoid(features, trueLabel, theta):
    
    m = trueLabel.size
    prediction = sigmoid(np.dot(features,theta))
    
    cost = (1.0/m) * np.dot((-trueLabel.T),np.log(prediction)) - np.dot((1-trueLabel).T,np.log(1-prediction)) # log-likelihood vector

    return cost
    

    

